# Fedora Post Install

I made this script specifically for the KDE spin, it can be edited for GNOME.

Reference: https://docs.fedoraproject.org/en-US/quick-docs/

Run `sudo ./FedoraPostInstallScript.sh`.

It will do some initial setup and than install software.

## Other Software RPM's

[ProtonVPN](https://protonvpn.com/support/official-linux-vpn-fedora/)

[MKVToolNIX](https://mkvtoolnix.download/downloads.html#fedora)

## Gnome

libadwaita theme ported to GTK-3: https://github.com/lassekongo83/adw-gtk3

Firefox Gnome Theme: https://github.com/rafaelmardojai/firefox-gnome-theme

Tela Icon Theme: https://github.com/vinceliuice/Tela-icon-theme

Tela Grub Theme: https://github.com/vinceliuice/grub2-themes

Mouse Cursur Theme: https://www.gnome-look.org/p/1197198

## Flatpak

Mouse cursors not showing correctly.

`flatpak --user override --filesystem=$HOME/.icons:ro`

## Finally

Upgrade system and reboot.

`sudo dnf upgrade -y --refresh && shutdown -r now`

After reboot, Fedora is ready to go.
