#!/bin/bash

#
# This is a script for a post install of Fedora.
# Made for Ashley
# Reference: https://docs.fedoraproject.org/en-US/quick-docs/
#

if [ "$EUID" -ne 0 ]
  then printf "Script requires root access.\nExiting now...\n"
  exit
fi

# Set Hostname
hostnamectl set-hostname --pretty "Fedora Blacked Out"
hostnamectl set-hostname --static fedora-blacked-out

# Keychron Function Key Fix (Ref: https://wiki.archlinux.org/title/Apple_Keyboard)
# fnmode: 0 = disabled, 1 = media keys first, 2 = function keys first
#printf "options hid_apple fnmode=2\n" >> /etc/modprobe.d/hid_apple.conf

# Add some configurations to dnf
printf "max_parallel_downloads=20 \ndeltarpm=True \ndefaultyes=True \n" >> /etc/dnf/dnf.conf

# Micro (Hands down the best terminal text editor)
# Clipboard (xclip for XOrg, wl-clipboard for Wayland)
dnf install -y micro xclip wl-clipboard

# Enable RPM Fusion
dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# Enable Media Codecs
dnf install -y gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
dnf install -y lame\* --exclude=lame-devel
dnf group upgrade -y --with-optional Multimedia

# Enable MESA Hardware Video Acceleration Drivers
dnf swap -y mesa-va-drivers mesa-va-drivers-freeworld
dnf swap -y mesa-vdpau-drivers mesa-vdpau-drivers-freeworld

# Firefox h264 Stuff (Enable by going to about:addons in Firefox)
dnf config-manager --set-enabled fedora-cisco-openh264
dnf install -y gstreamer1-plugin-openh264 mozilla-openh264

# Update Flatpak
dnf install -y flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak update

# System
dnf install -y \
git \
lm_sensors \
nfs-utils \
openssl \
smartmontools \
zfs-fuse \
unrar \
p7zip-plugins

# Utilities
dnf install -y \
corectrl \
keepassxc \
lgogdownloader \
transmission \
openrgb \
piper
#texlive-scheme-full

# Media Stuff
dnf install -y \
ffmpeg \
flac \
mpv \
youtube-dl

# Gaming
dnf install -y \
gamemode \
mangohud \
lutris \
steam

if [ "$DESKTOP_SESSION" = "plasma" ];
then
	# KDE (https://apps.kde.org/en-gb/)
	dnf install -y \
	audex \
	audiocd-kio \
	dolphin-plugins \
	elisa-player \
	filelight \
	gwenview \
	kate \
	kid3 \
	kolourpaint \
	krita \
	latte-dock \
	okular
fi

if [ "$DESKTOP_SESSION" = "gnome" ];
then
	dnf install -y \
	gnome-tweaks \
	lollypop \
	sound-juicer \
	soundconverter \
	gthumb \
	file-roller \
	webp-pixbuf-loader
	
	flatpak install -y flathub \
	com.mattjakeman.ExtensionManager
fi


# Flatpak Installs
flatpak install -y flathub \
com.discordapp.Discord \
com.makemkv.MakeMKV \
com.mojang.Minecraft \
com.spotify.Client \
com.github.tchx84.Flatseal
